﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_Gestion_CSGO
{
	class Equipe
	{
		public Guid id { get; set; }
		public string name { get; set; }
		public decimal totalMoneyWin { get; set; }
		public string majorSponsor { get; set; }
		public List<Joueur> listJoueur = new List<Joueur>();
		public static List<Equipe> listEquipe = new List<Equipe>();

		public Equipe() {
			this.id = Guid.NewGuid();
		}

		public Equipe(string name, decimal totalMoneyWin, string majorSponsor) {
			this.id = Guid.NewGuid();
			this.name = name;
			this.totalMoneyWin = totalMoneyWin;
			this.majorSponsor = majorSponsor;

			listEquipe.Add(this);
		}

		public Equipe(string name, decimal totalMoneyWin, string majorSponsor, List<Joueur> listJoueur)
		{
			//Vérification du nombre de joueur
			if (listJoueur.Count < 5 && listJoueur.Count > 7) throw new Exception();

			this.id = Guid.NewGuid();
			this.name = name;
			this.totalMoneyWin = totalMoneyWin;
			this.majorSponsor = majorSponsor;

			this.listJoueur = listJoueur;

			listEquipe.Add(this);
		}

		public void ajouteJoueur(Joueur j) {
			this.listJoueur.Add(j);
		}

		public void retireJoueur(Joueur j) {
			this.listJoueur.RemoveAll(x => x.pers.id == j.pers.id);
		}

		public void supprimeEquipe() {
			listEquipe.RemoveAll(x => x.id == this.id);
		}

	}
}
