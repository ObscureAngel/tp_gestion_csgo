﻿namespace TP_Gestion_CSGO
{
	partial class Accueil
	{
		/// <summary>
		/// Variable nécessaire au concepteur.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Nettoyage des ressources utilisées.
		/// </summary>
		/// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Code généré par le Concepteur Windows Form

		/// <summary>
		/// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette méthode avec l'éditeur de code.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Accueil));
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.accueilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.administrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.gestionDesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.gestionDesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.gestionDesToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
			this.gestionDesMatchsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.menuStrip1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.SuspendLayout();
			// 
			// pictureBox1
			// 
			this.pictureBox1.ImageLocation = "C:\\Users\\Maxime\\source\\repos\\TP_Gestion_CSGO\\TP_Gestion_CSGO\\img\\1145486_1.jpg";
			this.pictureBox1.Location = new System.Drawing.Point(542, 201);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(246, 237);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accueilToolStripMenuItem,
            this.administrationToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(800, 24);
			this.menuStrip1.TabIndex = 1;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// accueilToolStripMenuItem
			// 
			this.accueilToolStripMenuItem.Name = "accueilToolStripMenuItem";
			this.accueilToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
			this.accueilToolStripMenuItem.Text = "Accueil";
			// 
			// administrationToolStripMenuItem
			// 
			this.administrationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gestionDesToolStripMenuItem,
            this.gestionDesToolStripMenuItem1,
            this.gestionDesToolStripMenuItem2,
            this.gestionDesMatchsToolStripMenuItem});
			this.administrationToolStripMenuItem.Name = "administrationToolStripMenuItem";
			this.administrationToolStripMenuItem.Size = new System.Drawing.Size(98, 20);
			this.administrationToolStripMenuItem.Text = "Administration";
			// 
			// gestionDesToolStripMenuItem
			// 
			this.gestionDesToolStripMenuItem.Name = "gestionDesToolStripMenuItem";
			this.gestionDesToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
			this.gestionDesToolStripMenuItem.Text = "Gestion des joueurs";
			this.gestionDesToolStripMenuItem.Click += new System.EventHandler(this.gestionDesToolStripMenuItem_Click);
			// 
			// gestionDesToolStripMenuItem1
			// 
			this.gestionDesToolStripMenuItem1.Name = "gestionDesToolStripMenuItem1";
			this.gestionDesToolStripMenuItem1.Size = new System.Drawing.Size(222, 22);
			this.gestionDesToolStripMenuItem1.Text = "Gestion des équipes";
			this.gestionDesToolStripMenuItem1.Click += new System.EventHandler(this.gestionDesToolStripMenuItem1_Click);
			// 
			// gestionDesToolStripMenuItem2
			// 
			this.gestionDesToolStripMenuItem2.Name = "gestionDesToolStripMenuItem2";
			this.gestionDesToolStripMenuItem2.Size = new System.Drawing.Size(222, 22);
			this.gestionDesToolStripMenuItem2.Text = "Gestion des commentateurs";
			// 
			// gestionDesMatchsToolStripMenuItem
			// 
			this.gestionDesMatchsToolStripMenuItem.Name = "gestionDesMatchsToolStripMenuItem";
			this.gestionDesMatchsToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
			this.gestionDesMatchsToolStripMenuItem.Text = "Gestion des matchs";
			// 
			// dataGridView1
			// 
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Location = new System.Drawing.Point(12, 117);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.Size = new System.Drawing.Size(524, 321);
			this.dataGridView1.TabIndex = 2;
			// 
			// Accueil
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 450);
			this.Controls.Add(this.dataGridView1);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.menuStrip1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "Accueil";
			this.Text = "Accueil";
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem accueilToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem administrationToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem gestionDesToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem gestionDesToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem gestionDesToolStripMenuItem2;
		private System.Windows.Forms.ToolStripMenuItem gestionDesMatchsToolStripMenuItem;
		private System.Windows.Forms.DataGridView dataGridView1;
	}
}

