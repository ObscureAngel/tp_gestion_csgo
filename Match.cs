﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_Gestion_CSGO
{
	class Match
	{
		public Guid id { get; set; }
		public string name { get; set; }
		public DateTime date { get; set; }
		public decimal moneyPrize { get; set; }
		public bool winner { get; set; }
		public Equipe winnerTeam { get; set; }
		public static List<Match> listMatchs = new List<Match>();
		public List<Equipe> listTeams = new List<Equipe>();
		public List<Commentateur> listCaster = new List<Commentateur>();

		public Match() {
			this.id = Guid.NewGuid();
		}

		public Match (string name, DateTime date, decimal moneyPrize, List<Equipe> listTeams, List<Commentateur> listCaster) 
		{
			//Vérification du niveau des caster
			bool isOk = false;
			foreach (Commentateur c in listCaster)
			{
				if (moneyPrize >= 10000)
				{
					if (c.totalCast >= 5) isOk = true;
				}
			}
			if (!isOk) throw new Exception();

			//Vérification du nombre de caster
			if (listCaster.Count < 2 && listCaster.Count > 4) throw new Exception();

			//Vérification du nombre de joueur minimal pour la compétition
			isOk = true;
			foreach (Equipe e in listTeams) 
			{
				if (e.listJoueur.Count < 5) isOk = false;
			}
			if (!isOk) throw new Exception();

			//Vérification du nombre d'équipes
			if (listTeams.Count != 2) throw new Exception();


			this.id = Guid.NewGuid();
			this.name = name;
			this.date = date;
			this.moneyPrize = moneyPrize;

			this.listCaster = listCaster;
			this.listTeams = listTeams;

			listMatchs.Add(this);
		}

		public void ajouterCaster(Commentateur c) {
			//Vérification du niveau des caster
			if (moneyPrize >= 10000)
			{
				if (c.totalCast >= 5) throw new Exception();
			}

			this.listCaster.Add(c);
		}

		public void ajouterTeam(Equipe e) {
			//Vérification du nombre de joueur minimal pour la compétition
			if (e.listJoueur.Count < 5) throw new Exception();

			this.listTeams.Add(e);
		}

		public void matchGagner(Match m, Equipe e) {
			m.winner = true;
			m.winnerTeam = e;

			Equipe ePerdante = (Equipe) this.listTeams.Where(x => x != e);

			Classement.equipeGagne(e);
			Classement.equipePerd(ePerdante);

			foreach(Commentateur c in this.listCaster) {
				c.totalCast++;
			}
		}

		public void matchNul(Match m) {
			this.winner = false;

			foreach (Equipe e in this.listTeams) {
				Classement.equipeMatchNul(e);
			}

			foreach (Commentateur c in this.listCaster)
			{
				c.totalCast++;
			}
		}

		public void retireEquipe(Equipe e) {
			this.listTeams.RemoveAll(x => x.id == e.id);
		}

		public void retireCommentateur(Commentateur c) {
			this.listCaster.RemoveAll(x => x.id == c.id);
		}

		public void supprimeMatch() {
			listMatchs.RemoveAll(x => x.id == this.id);
		}
	}
}
