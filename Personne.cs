﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_Gestion_CSGO
{
	class Personne
	{
		public Guid id { get; set; }
		public string nom { get; set; }
		public string prenom { get; set; }
		public int age { get; set; }
		public string nationalite { get; set; }

		public Personne() {
			this.id = Guid.NewGuid();
		}

		public Personne (string nom, string prenom, int age, string nationalite) {
			if (age < 18) throw new Exception();

			this.id = Guid.NewGuid();
			this.nom = nom;
			this.prenom = prenom;
			this.age = age;
			this.nationalite = nationalite;
		}

	}
}
