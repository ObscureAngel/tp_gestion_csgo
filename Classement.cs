﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_Gestion_CSGO
{
	class Classement
	{
		private const int WIN = 3;
		private const int DRAW = 1;
		private const int LOOSE = 0;

		public int totalPoint { get; set; }
		public int draw { get; set; }
		public int win { get; set; }
		public int loose { get; set; }
		public Equipe equipe { get; set; }
		public static List<Classement> listClassement = new List<Classement>();

		public Classement() {
			this.win = 0;
			this.loose = 0;
			this.draw = 0;
			this.totalPoint = 0;
		}

		public Classement(Equipe equipe) {
			this.equipe = equipe;
			this.win = 0;
			this.loose = 0;
			this.draw = 0;
			this.totalPoint = 0;

			listClassement.Add(this);
		}

		public static void equipeGagne(Equipe gagne) {
			Classement c = (Classement) listClassement.Where(x => x.equipe == gagne);
			c.win++;
		}

		public static void equipeMatchNul(Equipe e) {
			Classement c = (Classement) listClassement.Where(x => x.equipe == e);
			c.draw++;
		}

		public static void equipePerd(Equipe perdante) {
			Classement c = (Classement) listClassement.Where(x => x.equipe == perdante);
			c.loose++;
		}

		public static int calculePoint (Equipe e) {
			Classement c = (Classement) listClassement.Where(x => x.equipe == e);
			int total = 0;

			total += c.loose * LOOSE;
			total += c.draw * DRAW;
			total += c.win * WIN;
			
			return total;
		}

		public void supprimeClassement() {
			listClassement.RemoveAll(x => x.equipe == this.equipe);
		}
	}
}
