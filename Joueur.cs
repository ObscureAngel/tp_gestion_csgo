﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_Gestion_CSGO
{
	class Joueur : Personne
	{
		public Personne pers;
		public string username { get; set; }
		public string inGameRole { get; set; }
		public double accuracy { get; set; }
		public int numberMVP { get; set; }
		public static List<Joueur> listJoueur = new List<Joueur>();

		public Joueur() {
			
		}

		public Joueur(Personne pers, string username, string inGameRole) {
			this.pers = pers;
			this.username = username;
			this.inGameRole = inGameRole;

			listJoueur.Add(this);
		}

		public Joueur(string nom, string prenom, int age, string nationalite, string username, string inGameRole) : base (nom, prenom, age, nationalite) {
			this.pers = new Personne(nom, prenom, age, nationalite);
			this.username = username;
			this.inGameRole = inGameRole;

			listJoueur.Add(this);
		}

		public void supprimeJoueur() {
			listJoueur.RemoveAll(x => x.id == this.id);
		}
	}
}
