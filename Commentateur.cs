﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_Gestion_CSGO
{
	class Commentateur : Personne
	{
		public Personne pers;
		public string casterName { get; set; }
		public int totalCast { get; set; }
		public int casterNote { get; set; }
		public static List<Commentateur> listCaster = new List<Commentateur>();

		public Commentateur() {

		}

		public Commentateur(Personne pers, string casterName, int totalCast, int casterNote) {
			this.pers = pers;
			this.casterName = casterName;
			this.totalCast = totalCast;
			this.casterNote = casterNote;

			listCaster.Add(this);
		}

		public Commentateur (string nom, string prenom, int age, string nationalite, string casterName, int totalCast, int casterNote) : base(nom, prenom, age, nationalite) {
			this.pers = new Personne(nom, prenom, age, nationalite);
			this.casterName = casterName;
			this.totalCast = totalCast;
			this.casterNote = casterNote;

			listCaster.Add(this);
		}

		public void supprimeCommentateur() {
			listCaster.RemoveAll(x => x.id == this.id);
		}
	}
}
